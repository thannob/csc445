// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.15;

import "./HelloWorld.sol";

contract HelloExtend is HelloWorld {
    function setMessage2(string memory _message) public {
        setMessage(string.concat(_message, "_1"));
    }
}
